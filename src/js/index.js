import { gsap } from "gsap"
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger'
import { ScrollToPlugin } from "gsap/ScrollToPlugin"


gsap.registerPlugin(ScrollTrigger)
gsap.registerPlugin(ScrollToPlugin);


window.addEventListener("load", function(){

    // on click go down

    let goDown = document.getElementsByClassName('hero__down')[0]

    goDown.addEventListener('click',function(){
        gsap.to(window, {duration: 1, scrollTo: "#start"});
    })


    // ....
    
    let animations = [],
        triggers = document.querySelectorAll('[data-animate]')

       
    for(let i=0; i< triggers.length; i++){

      let el = triggers[i], 
          animation = triggers[i].dataset.animate
      
      // content block
      if(animation == 'text'){
  
        let meta = el.getElementsByClassName('section__meta')[0],
            heading = el.getElementsByClassName('section__heading')[0],
            text = el.getElementsByClassName('section__text')[0]

       
       let  tl = gsap.timeline({
            paused: true,
            scrollTrigger: {
            //id: 'animation' + i,
            trigger: el,
            start: 'top 80%',
            //markers: true,
            toggleActions: 'play none none reverse'
            }
        })

        
        if(meta){
          tl.from(meta,{duration:0.4, autoAlpha:0, ease: 'power1.inOut'})
        }

        if(heading){
            tl.from(heading,{duration:0.4, autoAlpha:0, ease: 'power1.inOut'})
        }

        if(text){
            tl.from(text,{duration:0.4, autoAlpha:0, ease: 'power1.inOut'})
        }
       
        animations.push(tl)

      }

       // big-visual

       if(animation == 'big-visual'){
  
        let visual = el.getElementsByClassName('big-visual')[0]

       
        let  tl = gsap.timeline({
                paused: true,
                scrollTrigger: {
                //id: 'animation' + i,
                trigger: el,
                start: 'top 60%',
                //markers: true,
                toggleActions: 'play none none reverse'
                }
            })

        
        if(visual){
            tl.from(visual,{duration:0.6, yPercent:30, ease: 'power1.inOut'})
            tl.from(visual,{duration:0.6, autoAlpha:0, ease: 'power1.inOut'},'-=0.3')
        }

       
        animations.push(tl)

      }

       // hero

       if(animation == 'hero'){
  
        let logo = el.getElementsByClassName('logo')[0],
            heading = el.getElementsByClassName('hero__heading')[0],
            subline = el.getElementsByClassName('hero__subline')[0],
            opening = el.getElementsByClassName('hero__opening')[0]

       
        let  tl = gsap.timeline({
                paused: true,
                scrollTrigger: {
                //id: 'animation' + i,
                trigger: el,
                start: 'top 60%',
                //markers: true,
                toggleActions: 'play none none reverse'
                }
            })

        tl.addLabel('start')
        
        if(logo){
            tl.from(logo,{duration:0.6, yPercent:30, ease: 'power1.inOut'},'start+=0.3')
            tl.from(logo,{duration:0.6, autoAlpha:0, ease: 'power1.inOut'},'-=0.4')
        }
       
        if(heading){
            tl.from(heading,{duration:0.6, yPercent:30, ease: 'power1.linear'},'start')
            tl.from(heading,{duration:0.6, autoAlpha:0, ease: 'power1.inOut'},'-=0.4')
        }
     
        if(subline){
            tl.from(subline,{duration:0.6, yPercent:30, ease: 'power1.inOut'},'start')
            tl.from(subline,{duration:0.6, autoAlpha:0, ease: 'power1.inOut'},'-=0.3')
        }

        if(opening){
            tl.from(opening,{duration:0.6, yPercent:30, ease: 'power1.inOut'},'start')
            tl.from(opening,{duration:0.6, autoAlpha:0, ease: 'power1.inOut'},'-=0.3')
        }

       
        animations.push(tl)

      }


       // content block
       if(animation == 'about'){
  
        let meta = el.getElementsByClassName('section__meta')[0],
            heading = el.getElementsByClassName('section__heading')[0],
            text = el.getElementsByClassName('section__text')[0],
            visual1 = el.getElementsByClassName('a-visual-1'),
            visual2 = el.getElementsByClassName('a-visual-2')

       
       let  tl = gsap.timeline({
            paused: true,
            scrollTrigger: {
            //id: 'animation' + i,
            trigger: el,
            start: 'top 50%',
            //markers: true,
            toggleActions: 'play none none reverse'
            }
        })

        tl.addLabel('start')

        
        if(meta){
          tl.from(meta,{duration:0.4, autoAlpha:0, ease: 'power1.inOut'})
        }

        if(heading){
            tl.from(heading,{duration:0.4, autoAlpha:0, ease: 'power1.inOut'})
        }

        if(text){
            tl.from(text,{duration:0.4, autoAlpha:0, ease: 'power1.inOut'})
        }


        if(visual1){
            tl.from(visual1,{duration:0.6, autoAlpha:0, yPercent:100, rotate:0, ease: 'power1.inOut'},'start+=0.2')
        }
       
        if(visual2){
            tl.from(visual2,{duration:0.6, autoAlpha:0, rotate:0, yPercent:100, ease: 'power1.inOut'},'start+=0.6')
        }
       

        
        animations.push(tl)

      }

    
    }




});



